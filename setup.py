from setuptools import setup
from setuptools import find_packages

# with open("README.md", "r") as fh:
#     long_description = fh.read()

setup(
    name="yapc",
    version="0.0.1",
    description='Yet Another Phonon Code',
    # long_description=long_description,
    # long_description_content_type="text/markdown",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=['numpy', 'mdb', 'pint'],
    extras_require={
        'develop': ['python-language-server[all]',]
    },
    entry_points={
        'console_scripts': [
        ],
    },
    author="",
    author_email="",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
