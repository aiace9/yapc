# Yet Another Phonon Code

This code is simple library to compute crystalline phonons. Forces can be
computed with any code you prefer, you just need to parse them correctly.

No symmetry analysis is performed/exploited at the moment.

It does not offer many features, and to use it you need MDB package. Such
package is not yet public so please message me to get a working copy.

# Installation
clone the repository, then in the main folder.

```bash
git clone 
pip install -e .
```

# Usage

For now please refer to the two notebooks in the doc folder
- creator contains the instructions to create the a strucutre,
- usage contains an example for the usage.
