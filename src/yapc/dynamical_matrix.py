"""Routines for the dynamical matrix computation.

These is the hart of the code.
"""
import logging

import time
import itertools
from typing import Tuple
from collections.abc import Sequence

import numpy as np

from mdb.configuration import Configuration

logger = logging.getLogger(__name__)


class Phonon():
    """Callable class to compute the dynamical matrix.

    Parameters
    ----------
    conf: unit cell configuration
    replicas: number of unit cell replicas in each direction.


    Example
    -------
    ``` python
    p = Phon(conf, (3,3,3))
    p.dynamical_matrix_real(confs)

    dyn_mat = p()
    w, v = np.linalg.eig(d_mat)
    ```

    where in the first step we initiated the class with:
    - conf is a configuration of the used unit cell
    - 3,3,3 is a vector containing the number of replicas

    In the second step we initiated the dynamical matrix passing the displaced
    configurations (check the doc on how to).

    Finally we have computed the dynamical matrix at gamma and the
    eigenvalues/eigenvectors.
    """

    def __init__(self, conf: Configuration, replicas: Tuple[int, int, int]):
        self._conf = conf
        self._n_atoms = conf.n_atoms
        self._replicas = replicas
        self._jump = replicas[0] * replicas[1] * replicas[2]
        self._superconf = conf._multiply(*replicas)

        # setup internal quantities
        self._performance = {}
        self._compute_all_displaced()
        self._compute_weights()
        self._performance['calls'] = []

    def dynamical_matrix_real(self,
                              confs: Sequence[Tuple[Configuration,
                                                    Configuration]],
                              eps: float = 0.01,
                              force_modifier=lambda x: x,
                              acoustic_sum_rule: bool = True):
        """Build the dynamical matrix in real space.

        Parameters
        ----------
        confs: **ordered** list of couple of configurations.
               The order is the same created by displacer call.
               So: for each atom in the unit cell plus and minus
               the coordinates in the sequence x, y, z
               [[+x, -x], # atom 1
                [+y, -y], # atom 1
                [+z, -z], # atom 1
                [+x, -x], # atom 2
                ...]
        eps: displacement.
        force_modifier: a function to be applied to the forces before
                        the computation of the second derivative.
                        eg: a rotation.
        acoustic_sum_rule: apply the acoustic sum rule if needed.
        """
        dyn_mat_r = []
        for c, mc in confs:
            f = force_modifier(c.forces)
            mf = force_modifier(mc.forces)
            der = (mf - f) / (2 * eps)
            dyn_mat_r.append(der)

        self._dyn_mat_r = np.array(dyn_mat_r).reshape(self._n_atoms, 3,
                                                      self._n_atoms,
                                                      self._jump, 3)
        if self.require_acoustic_sum_rule and acoustic_sum_rule:
            logger.warning('applying acoustic sum rule 101, '
                           'this is only for translation not rotations')
            before, after = self.apply_acoustic_sum_rule()
            logger.info('acoustic delta went from %e to %e', before, after)
        return self._dyn_mat_r

    def _acoustic_delta(self):
        """Measure the max error for the low frequency modes."""
        dyn_mat = self.dyn_mat()
        # contract over nb
        return np.abs(dyn_mat.sum(axis=-2)).max()

    @property
    def require_acoustic_sum_rule(self) -> bool:
        """Check if the acoustic sum rule is needed."""
        max_delta = self._acoustic_delta()
        if max_delta > 1e-6:
            logger.warning('sum over all atoms b is not zero: %f', max_delta)
        # 1e-4 is A LOOSE tolerance. But it is what I see for SW
        return max_delta > 1e-4

    def apply_acoustic_sum_rule(self) -> Tuple[float, float]:
        """Apply a basic acoustic sum rule.

        This call modify the internal status of the dynamical matrix
        in real space.

        For theoretical doc check the Quantum Espresso.

        Returns
        -------
        the acoustic delta before and after the application of the acoustic
        sum rule.
        """
        tmp = self._dyn_mat_r.sum(axis=(2, 3))
        before = self._acoustic_delta()
        for i in range(self._n_atoms):
            self._dyn_mat_r[i, :, i,
                            0, :] = self._dyn_mat_r[i, :, i, 0, :] - tmp[i]
        after = self._acoustic_delta()
        return before, after

    def _compute_all_displaced(self):
        """Set self._all_disp.

        This is a vector of shape n_atom, n_atom, n_replicas, 3
        That has the as first index the centered atom in the unit cell.
        Second and third index combined count for all the atoms in the
        super-cell. Second index can be interpreted as atoms location in
        the elementary cell, while third index is the replica location.
        Last index are the three crystalline coordinates w.r.t.
        the unit cell and mapped in the WS cell of the super-cell.
        """
        pos_cry = self._superconf.pos_crystalline * self._replicas
        # extract the position of the unit cell atoms
        # n_at, 3
        unit_cell_atoms = pos_cry[::self._jump]
        # zero every one of the n_atoms
        # final tensor is
        # n_at, n_at x n_replicas, 3
        # crystalline coordinates wrt the cell
        all_disp = pos_cry[np.newaxis, :] - unit_cell_atoms[:, np.newaxis, :]

        # move all the atoms in the ws super-cell
        for idx, r in enumerate(self._replicas):
            all_disp[..., idx][all_disp[..., idx] > r / 2] -= r

        # reshape to allow for easier contraction.
        self._all_disp = all_disp.reshape(self._n_atoms, self._n_atoms,
                                          self._jump, 3)

    def _compute_weights(self, eps: float = 1e-6):
        """Resolution of the weights.

        Aka for each atom that fells on the surface of the WS super-cell
        we compute the corresponding degeneracy and displacement vectors to
        obtain the phase of the degenerate atoms.

        Parameters
        ----------
        eps: define the "on the surface" concept.
             A crust with 2 * eps width is used.

        Outcome
        -------
        set: 
        * self._indexes: array, n_denerate atoms, 3
            store the location in the self._all_disp array of
            degenerate atoms,
            - first index refer to central atom
            - second index to location in the basis
            - third index to replica
        * self._degeneracies: array, len(self._index)
            for each index store the degeneracy of the atom.
        * self._trans_vectors: list of len (self._index)
            for each index store the translation vectors in super-cell
            crystalline coordinates to obtain the equivalent replicas
        """
        t = time.time()
        # copute the cutoff in real space for each atom
        cs = np.linalg.norm(self._all_disp @ self._conf.cell.lattice_vectors,
                            axis=-1)
        # extract the upper and lower bound of the replicas index needed to
        # obtain all the replicas within the cutoff
        inv_supercell = self._superconf.cell.reciprocal
        all_disp_sup = self._all_disp @ self._conf.cell.lattice_vectors \
            @ inv_supercell
        # crystalline coordinates wrt the super-cell
        lbs = all_disp_sup - cs[..., np.newaxis] *\
            np.linalg.norm(inv_supercell, axis=-1)
        lbs = lbs.round().astype(int)
        ubs = all_disp_sup + cs[..., np.newaxis] *\
            np.linalg.norm(inv_supercell, axis=-1)
        ubs = ubs.round().astype(int)

        indexes = []
        degeneracies = []
        trans_vectors = []
        # This loop iterate over one atom at a time
        # optimization can for sure be added here
        for x in range(lbs.shape[0]):
            for y in range(lbs.shape[1]):
                for z in range(lbs.shape[2]):
                    l_min, m_min, n_min = lbs[x, y, z]
                    l_max, m_max, n_max = ubs[x, y, z]
                    l_list = range(l_min, l_max + 1)
                    m_list = range(m_min, m_max + 1)
                    n_list = range(n_min, n_max + 1)
                    iters = np.asarray(
                        list(itertools.product(l_list, m_list, n_list)))
                    # generate all the replicas
                    ctest = np.linalg.norm(
                        (all_disp_sup[x, y, z] - iters)
                        @ self._superconf.cell.lattice_vectors,
                        axis=-1)
                    if (ctest < cs[x, y, z] - eps).any():
                        # One of the point is inside the ws cell!
                        continue
                    mask = (ctest < cs[x, y, z] + eps)
                    deg = mask.sum()
                    if deg > 1:
                        indexes.append((x, y, z))
                        degeneracies.append(deg)
                        trans_vectors.append(
                            iters[mask][(iters[mask] != np.array(
                                (0, 0, 0))).any(axis=-1)])
                    elif deg == 0:
                        raise ValueError(
                            'Point %i %i %i is outside the WS cell'
                            'This should not happen by construction', x, y, z)
        self._indexes = np.array(indexes)
        self._degeneracies = np.array(degeneracies)
        self._trans_vectors = trans_vectors
        self._performance['compute_weights'] = time.time() - t

    def __call__(self, q: np.array = np.array((0, 0, 0))):
        """Compute and diagonalize dynamical matrix."""
        return np.linalg.eig(self.dyn_mat(q))

    def dyn_mat(self, q: np.array = np.array((0, 0, 0))):
        r"""Compute dynamical matrix at a given q.

        During the execution if q != $\gamma$ the time spent is stored.

        Parameters
        ----------
        q: vector (or sequence of vectors) in the reciprocal space
           (you must multiply it by 2pi, because the reciprocal cell
            is just inv(lattice vectors))

        Returns
        -------
        The dynamical matrix for the given q.

        """

        if len(q.shape) == 1:
            q = q[np.newaxis, :]

        if q.shape[0] == 1 and np.isclose(q[0], (0, 0, 0)).all():
            # Phase is irrelevant. We don't need to waste time
            # Since this computation is not representative of the time
            # spent in this routine we avoid logging it.
            dyn_mat = self._dyn_mat_r.sum(axis=-2)
            dyn_mat = dyn_mat.reshape(self._n_atoms * 3, self._n_atoms * 3)
            return dyn_mat

        # First part of the phase computation:
        cell = self._conf.cell.lattice_vectors
        supercell = self._superconf.cell.lattice_vectors

        q_expended = q[:, np.newaxis, np.newaxis, np.newaxis]
        exp_term = np.sum(
            (self._all_disp @ cell)[np.newaxis, ...] * q_expended, axis=-1)
        phase = np.exp(1j * exp_term)
        t = time.time()
        # Second part of the phase computation:
        phase_corrs = []
        for idx, deg, trans_vector in zip(self._indexes, self._degeneracies,
                                          self._trans_vectors):
            loc = self._all_disp[idx[0], idx[1], idx[2]]
            exp_term = np.sum(
                (loc @ cell - trans_vector @ supercell) * q[:, np.newaxis],
                axis=-1)
            tmp = np.exp(1j * exp_term)
            phase_corr = tmp.sum(axis=-1) / deg
            phase_corrs.append(phase_corr)
        phase_corrs = np.array(phase_corrs).T

        view = phase[:, self._indexes[:, 0], self._indexes[:, 1],
                     self._indexes[:, 2]]
        view /= self._degeneracies
        view += phase_corrs
        phase[:, self._indexes[:, 0], self._indexes[:, 1],
              self._indexes[:, 2]] = view
        dyn_mat = (self._dyn_mat_r *
                   phase[:, :, np.newaxis, ..., np.newaxis]).sum(axis=-2)
        dyn_mat = dyn_mat.reshape(q.shape[0], self._n_atoms * 3,
                                  self._n_atoms * 3)
        self._performance['calls'].append(time.time() - t)

        if q.shape[0] == 1:
            return dyn_mat[0]
        return dyn_mat

    @property
    def performance(self):
        s = "compute weights: "
        s += f"{self._performance['compute_weights'] * 1000 :2.2f} msec\n"
        calls = self._performance['calls']
        s += f"call: {np.mean(calls)*1000:2.2f} \pm {np.std(calls)*1000:2.2f} msec\n"
        s += f"average over {len(calls)} samples"
        return s

    def predict_time(self, calls: int):
        return np.mean(self._performance['calls']) * calls
