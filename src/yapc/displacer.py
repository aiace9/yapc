"""Routines to displace the atoms.

writing operations are left to the user
"""

from copy import deepcopy
from typing import Tuple
from collections.abc import Sequence

import numpy as np

from mdb.configuration import Configuration


def displace(conf: Configuration,
             replicas: Tuple[int, int, int],
             eps: float = 0.01) -> Sequence[Tuple[int, str, Configuration]]:
    """Displace the atoms.

    Parameters
    ----------
    conf: configuration (supercell version)
    replicas: number of unit cell replicas in each direction.
    eps: displacement

    Returns
    -------
    Sequence of Tuple
    - idx of the moved atom
    - direction (string)
    - configuration
    """
    dx = np.array([1, 0, 0]) * eps
    dy = np.array([0, 1, 0]) * eps
    dz = np.array([0, 0, 1]) * eps

    n_replicas = replicas[0] * replicas[1] * replicas[2]

    confs_tobe_comp = []
    for x in range(int(conf.n_atoms/n_replicas)):
        for label, j in [('x', dx), ('y', dy), ('z', dz)]:
            tmp_conf = deepcopy(conf)
            tmp_conf.displace_atom(n_replicas * x, j)
            confs_tobe_comp.append((x, label, tmp_conf))
            tmp_conf = deepcopy(conf)
            tmp_conf.displace_atom(n_replicas * x, -j)
            confs_tobe_comp.append((x, f'-{label}', tmp_conf))
    return confs_tobe_comp
