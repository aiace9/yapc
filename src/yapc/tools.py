"""Collection of random tools."""

import numpy as np

from mdb.configuration import Cell
from mdb.tools.lammps import convert_vectors_from_lmp


class LammpsForceModifier():
    """Modifier to convert force according to lammps.

    Parameters
    ----------
    cell: the super cell used for the calculation.
    """

    def __init__(self, cell: Cell):
        self._cell = cell

    def __call__(self, forces: np.array):
        """Convert forces back to a normal basis :).

        Parameters
        ----------
        forces: array (n_atoms, 3) as read form lammps

        Returns
        -------
        array (n_atoms, 3) rotated accordingly to the provided basis
        """
        return convert_vectors_from_lmp(self._cell.lattice_vectors, forces)
