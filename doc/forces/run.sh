#! /bin/bash

for i in $(find -name pos.in)
do
	  i="$(dirname $i)";
    echo "computing: $i"
	  cd $i;
    rm forces.dat lammps.out log.lammps -rf
    if [[ -f forces.dat ]]; then
        echo "already done"
	      cd ../.. > /dev/null;
        continue
    fi
    ln -s ../../in.lammps
    ln -s ../../Si.sw
    mpirun -np 4 lmp -in in.lammps >> lammps.out
    # tail -n 225 relax.atom > last_step.atom
	  cd ../../ > /dev/null;
done
